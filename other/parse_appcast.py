import copy
import pathlib

from lxml import etree

"""
etree remove element
"""

inpath = pathlib.Path("appcasts/smphdosx.xml")
outpath = pathlib.Path("out.xml")
outpath = inpath

parser = etree.XMLParser(strip_cdata=False)

with open(inpath, "rb") as source:
    tree = etree.parse(source, parser=parser)

root = tree.getroot()
channel = root.find("./channel")
link = root.find("./channel/link")
description = root.find("./channel/description")
lang = root.find("./channel/language")

items = channel.findall(".//item")
item = items[0]
item2 = copy.deepcopy(item)
lang.addnext(item2)

tree.write(str(outpath), xml_declaration=True, encoding="utf-8", method="xml")
