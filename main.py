import datetime
import os
import pathlib
import shutil
import subprocess
import zipfile

import dateutil.parser
import jinja2
import lxml.etree
import pytz
import requests
import yaml

cwd = pathlib.Path(os.getcwd())
build_dir = cwd / pathlib.Path("build")
app_dir = cwd / pathlib.Path("app")
keys_dir = cwd / pathlib.Path("keys")
priv_key = keys_dir / pathlib.Path("dsa_priv.pem")
pub_key = keys_dir / pathlib.Path("dsa_pub.pem")
dsa_param = keys_dir / pathlib.Path("dsaparam.pem")
appcasts_dir = cwd / pathlib.Path("appcasts")
tidy_config = cwd / pathlib.Path("tidy.config")
dmg_resources_dir = cwd / pathlib.Path("resources")
dmg_stage_basedir = build_dir / pathlib.Path("dmg_stage")

build_dir.mkdir(parents=True, exist_ok=True)
keys_dir.mkdir(parents=True, exist_ok=True)
appcasts_dir.mkdir(parents=True, exist_ok=True)


def remove(path):
    """ param <path> could either be relative or absolute. """
    if os.path.isfile(path) or os.path.islink(path):
        os.remove(path)  # remove the file
    elif os.path.isdir(path):
        shutil.rmtree(path)  # remove dir and all contains
    else:
        # directory/fill already gone
        pass


def tidy(path):
    cmd = f"tidy -config {tidy_config} -xml {path}"
    process = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
        universal_newlines=True,
    )
    stdout, stderr = process.communicate()
    assert stderr == ""


if not pub_key.exists():
    result = subprocess.Popen(
        f"/usr/bin/openssl dsaparam 4906 </dev/urandom >{dsa_param}",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
        cwd=keys_dir,
        universal_newlines=True,
    )
    rev = result.communicate()[0].partition("\n")[0]

    result = subprocess.Popen(
        f"/usr/bin/openssl gendsa {dsa_param} -out {priv_key}",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
        cwd=keys_dir,
        universal_newlines=True,
    )
    rev = result.communicate()[0].partition("\n")[0]
    result = subprocess.Popen(
        f"/usr/bin/openssl dsa -in {priv_key} -pubout -out {pub_key}",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
        cwd=keys_dir,
        universal_newlines=True,
    )
    rev = result.communicate()[0].partition("\n")[0]

with open("meta.yml", "r") as stream:
    meta = list(yaml.safe_load_all(stream))

for idx, dct in enumerate(meta, 1):
    app = pathlib.Path(dct["app"])
    version = dct["release_version"]
    sources = dct["source"]
    for source in sources:
        p = pathlib.Path(source.replace("@RELEASE_VERSION@", version))
        _zip = cwd / app_dir / p
        if _zip.exists():
            break
    extract = cwd / app_dir / app
    if extract.exists():
        extract.unlink()
    if not _zip.exists():
        raise ValueError(f"Can't find {_zip}")
    with zipfile.ZipFile(_zip, "r") as zipObj:
        os.chdir(build_dir)
        zipObj.extractall()

    # overwrite with my pub key
    path = build_dir / app / pathlib.Path("Contents/Resources/dsa_pub.pem")
    path.unlink()
    shutil.copy(pub_key, path)
    assert path.exists()

    # create zip file
    os.chdir(build_dir)
    target = build_dir / pathlib.Path(
        dct["target"].replace("@RELEASE_VERSION@", version)
    )
    zipf = zipfile.ZipFile(target, "w", zipfile.ZIP_DEFLATED)
    for root, dirs, files in os.walk(app):
        for file in files:
            zipf.write(os.path.join(root, file))
    zipf.close()

    # get new zipfile size
    cmd = f"stat --format='%s' {target}"
    process = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
        cwd=build_dir,
        universal_newlines=True,
    )
    stdout, stderr = process.communicate()
    zipfile_size = int(stdout)
    assert stdout
    assert stderr == ""

    cmd = f"""
    /usr/bin/openssl dgst -sha1 -binary <{target} |
       /usr/bin/openssl dgst -dss1 -sign {priv_key} |
       /usr/bin/openssl enc -base64 | tr -d '\n' | tr -d '\r'
    """

    process = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
        cwd=build_dir,
        universal_newlines=True,
    )
    stdout, stderr = process.communicate()
    dsa_signature = stdout
    assert dsa_signature
    assert stderr == ""

    def basename(path_str): return os.path.basename(path_str)

    env = jinja2.Environment(loader=jinja2.BaseLoader)
    env.filters["basename"] = basename
    tpl = env.from_string(
        """{#- jinja2 -#}
<rss version="2.0"
     xmlns:sparkle="http://www.andymatuschak.org/xml-namespaces/sparkle"
     xmlns:dc="http://purl.org/dc/elements/1.1/">
<item>
  <title>{{release_version}}</title>
  <sparkle:releaseNotesLink>http://update.streambox.com/updater/{{updater_basedir}}/{{release_version}}/{{changelog|basename}}</sparkle:releaseNotesLink>
  <pubDate>{{publish_date}}</pubDate>
  <enclosure url="http://update.streambox.com/updater/{{updater_basedir}}/{{release_version}}/{{zip_filename}}"
             sparkle:shortVersionString="{{release_version}}"
             sparkle:version="{{release_version}}"
             length="{{length}}"
             type="application/octet-stream"
             sparkle:dsaSignature="{{dsa_signature}}" />
  <description>
new stuff here please
  </description>
</item>
</rss>
    """
    )

    dct1 = {
        "release_version": dct["release_version"],
        "updater_basedir": dct["updater_basedir"],
        "changelog": dct["changelog"].replace(
            "@RELEASE_VERSION@", dct["release_version"]
        ),
        "publish_date": datetime.datetime.today().strftime("%a, %d %b %Y"),
        "zip_filename": target.name,
        "length": zipfile_size,
        "dsa_signature": dsa_signature,
    }

    out = build_dir / pathlib.Path(dct["sparkle_item"])
    this_release_appcast = tpl.render(dct1)
    open(out, "w").write(this_release_appcast)

    url = dct["appcast"]
    r = requests.head(url)

    filename = pathlib.Path(dct["appcast"]).name
    local_appcast = appcasts_dir / filename

    url_time = r.headers["last-modified"]
    url_date = dateutil.parser.parse(url_time)
    tz = pytz.timezone("US/Pacific")

    def myfetch(url, write_path):
        r = requests.get(url, allow_redirects=True)
        write_path.write_text(r.content.decode())

    if not local_appcast.exists():
        myfetch(url, local_appcast)
    else:
        file_time = datetime.datetime.fromtimestamp(
            os.path.getmtime(local_appcast)
        ).astimezone(tz)

        if url_date > file_time:
            myfetch(url, write_path)

    # add new release to appcast

    # parse appcast name from url
    ac = pathlib.Path(dct["appcast"]).name
    inpath = appcasts_dir / pathlib.Path(ac)
    outpath = inpath

    parser = lxml.etree.XMLParser(strip_cdata=False)

    with open(inpath, "rb") as source:
        tree = lxml.etree.parse(source, parser=parser)

    root = tree.getroot()
    channel = root.find("./channel")
    link = root.find("./channel/link")
    description = root.find("./channel/description")
    lang = root.find("./channel/language")

    new_release_appcast_item = lxml.etree.fromstring(this_release_appcast)
    item = new_release_appcast_item.find("./item")

    lang.addnext(item)
    tree.write(str(outpath), xml_declaration=True, encoding="utf-8", method="xml")
    tidy(local_appcast)

    # make dmg
    app_path = build_dir / pathlib.Path(dct["app"])
    dmg_stage = dmg_stage_basedir / pathlib.Path(dct["app"])
    if not app_path.exists():
        raise ValueError(f"can't find {app_path}")
    remove(dmg_stage_basedir)
    dmg_stage_basedir.mkdir(parents=True, exist_ok=False)
    shutil.copytree(app_path, dmg_stage)

    dmg_path = build_dir / pathlib.Path(dct["dmg"])
    if dmg_path.exists():
        dmg_path.unlink()
    cmd = [
        "create-dmg",
        "--volname",
        "Application Installer",
        "--background",
        str(
            dmg_resources_dir / pathlib.Path("Streambox Install Background 800x400.png")
        ),
        "--window-pos",
        "150",
        "100",
        "--text-size",
        "11",
        "--window-size",
        "800",
        "400",
        "--icon-size",
        "70",
        "--app-drop-link",
        "580",
        "260",
        "--icon",
        dct["app"],
        "220",
        "260",
        "--hide-extension",
        dct["app"],
        str(dmg_path),
        str(dmg_stage),
    ]
    # debug
    for idx, elem in enumerate(cmd):
        if " " in elem:
            print(f"'{elem}' ", end="")
        else:
            print(f"{elem} ", end="")
        if idx == len(cmd) - 1:
            print("")

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    p.communicate()
