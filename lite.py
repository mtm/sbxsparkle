import logging
import os
import pathlib
import stat
import subprocess

import dateutil.parser
import jinja2
import yaml

logger = logging.getLogger(pathlib.Path(__file__).stem)
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler(f"{pathlib.Path(__file__).stem}.log")
fh.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)

formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
ch.setFormatter(formatter)
fh.setFormatter(formatter)

logger.addHandler(fh)
logger.addHandler(ch)


env = jinja2.Environment(
    #    loader=jinja2.PackageLoader("stuff", "templates"),
    loader=jinja2.FileSystemLoader("templates"),
    autoescape=jinja2.select_autoescape(["html", "xml"]),
)


path = pathlib.Path("meta.yml")

with open(path, "r") as stream:
    docs = list(yaml.safe_load_all(stream))

for bdoc in docs:  # binary yaml document
    clyp = pathlib.Path(bdoc["changelog_yaml"])  # changelog yaml path
    with open(clyp, "r") as stream:
        vdocs = list(yaml.safe_load_all(stream))  # versions yaml document list

    for vdoc in vdocs:
        dt = dateutil.parser.parse(vdoc["date"])
        vdoc["date"] = dt.strftime("%b %-d, %Y")

    clt = env.get_template("changelog.html")  # changelog template
    clop = pathlib.Path(bdoc["changelog"])  # changlog output path
    clop.parent.mkdir(parents=True, exist_ok=True)

    open(clop, "w").write(clt.render(versions=vdocs))

    cmd = f"tidy -config tidy.config -asxhtml {str(clop)}"
    process = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
        universal_newlines=True,
    )
    stdout, stderr = process.communicate()
    logger.debug(stderr)

    def basename(path_str):
        return os.path.basename(path_str)

    up_script = pathlib.Path(bdoc["upload_script"])
    env2 = jinja2.Environment(loader=jinja2.BaseLoader)
    env2.filters["basename"] = basename
    tpl = env2.from_string(
        """{#- jinja2 -#}
#!/bin/bash

ssh -T update<<\EOF
mkdir -p /var/www/updater/{{bin.updater_basedir}}/{{bin.release_version}}
mkdir -p /var/www/updater/{{bin.updater_basedir}}/latest
EOF

scp build/{{bin.target.replace('@RELEASE_VERSION@',bin.release_version)}} build/{{bin.dmg}} {{bin.changelog}} update:/var/www/updater/{{bin.updater_basedir}}/{{bin.release_version}}

ssh -T update<<\EOF
cd /var/www/updater/{{bin.updater_basedir}}/latest
ln -fs ../{{bin.release_version}}/{{bin.dmg}} {{bin.dmg}}
ln -fs ../{{bin.release_version}}/{{bin.changelog|basename}} {{bin.changelog|basename}}
EOF

""")
    open(up_script, "w").write(tpl.render(bin=bdoc))
    up_script.chmod(path.stat().st_mode | stat.S_IEXEC)
