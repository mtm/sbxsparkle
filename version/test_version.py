import unittest


class TestNumberedVersion(unittest.TestCase):
    def setUp(self):
        self.v = NumberedVersion()

    def test_compile_pattern(self):
        p = self.v._compile_pattern(".", ["a", "b"])
        tests = {
            "1.2.3": True,
            "1a0": True,
            "1": True,
            "1.2.3.4a5": True,
            "b": False,
            "1c0": False,
            " 1": False,
            "": False,
        }
        for test, result in tests.iteritems():
            self.assertEqual(
                result,
                p.match(test) is not None,
                "test: {} result: {}".format(test, result),
            )

    def test_parse(self):
        tests = {"1.2.3.4a5": ((1, 2, 3, 4), ("a", 5))}
        for test, result in tests.iteritems():
            self.v.parse(test)
            self.assertEqual(result, (self.v.version, self.v.prerelease))

    def test_str(self):
        tests = (("1.2.3",), ("10-2-42rc12", "-", ["rc"]))
        for t in tests:
            self.assertEqual(t[0], str(NumberedVersion(*t)))

    def test_repr(self):
        v = NumberedVersion("1,2,3rc4", ",", ["lol", "rc"])
        expected = "NumberedVersion ('1,2,3rc4', ',', ['lol', 'rc'])"
        self.assertEqual(expected, repr(v))

    def test_order(self):
        test = ["1.7.0", "1.7.0rc0", "1.11.0"]
        expected = ["1.7.0rc0", "1.7.0", "1.11.0"]
        versions = [NumberedVersion(v, ".", ["rc"]) for v in test]
        self.assertEqual(expected, list(map(str, sorted(versions))))


if __name__ == "__main__":
    unittest.main()
