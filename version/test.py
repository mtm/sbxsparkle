import version

versions = ["1.7.0", "1.7.0rc2", "1.7.0rc1", "1.7.1", "1.11.0"]
l = sorted(versions, key=lambda v: version.NumberedVersion(v, ".", ["rc"]))
print(l)
